# Copyright (C) 2016 Léo Andrès <leo@ndrs.fr>
# Copyright (C) 2016 Anthony Chaput <thonyc4@openmailbox.org>
# Copyright (C) 2016 Berthault Justin <justin.berthault@zaclys.net>
# Copyright (C) 2016 Quentin Daem <quentin.daem@gmail.com>
# This file is distributed under the same license as the gnome-shell-extension-radio package.
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-radio 1.6\n"
"Report-Msgid-Bugs-To: hslbck@gmail.com\n"
"POT-Creation-Date: 2017-08-31 19:23+0200\n"
"PO-Revision-Date: 2016-06-15 22:05+0200\n"
"Last-Translator: Léo Andrès <leo@ndrs.fr>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#: prefs.js:49
msgid "Show title notifications"
msgstr "Afficher les notifications du titre à l'écoute"

#: prefs.js:65
#, fuzzy
msgid "Show title notification in the panel"
msgstr "Afficher les notifications du titre à l'écoute"

#: prefs.js:81
msgid "Enable Play/Stop Media Keys"
msgstr "Activer les touches démarrer/arrêter le multimedia"

#: prefs.js:97
msgid "Show volume adjustment slider in menu"
msgstr ""

#: channelListDialog.js:53
msgid "List of Channels"
msgstr "Liste des Canaux"

#: channelListDialog.js:57
msgid "Manage your radio stations"
msgstr "Gérer vos stations de radio"

#: channelListDialog.js:93 addChannelDialog.js:106 searchDialog.js:108
msgid "Cancel"
msgstr "Annuler"

#: channelListDialog.js:100
msgid "Delete"
msgstr "Supprimer"

#: channelListDialog.js:108
msgid "Edit"
msgstr "Modifier"

#: channelListDialog.js:117
msgid "Play"
msgstr "Lecture"

#: addChannelDialog.js:44
msgid "Channel Name"
msgstr "Nom du Canal"

#: addChannelDialog.js:63
msgid "Stream Address"
msgstr "Adresse du flux"

#: addChannelDialog.js:82
msgid "Charset (optional)"
msgstr "Jeu de caractères (facultatif)"

#: addChannelDialog.js:111 searchDialog.js:114
msgid "Add"
msgstr "Ajouter"

#: searchDialog.js:47
msgid "Browse "
msgstr "Parcourir"

#: searchDialog.js:68
msgid "Search"
msgstr "Rechercher"

#: searchDialog.js:156
msgid "No radio station found!"
msgstr "Aucune station de radio trouvée"

#: searchDialog.js:159
msgid "Server returned status code"
msgstr "Le serveur a renvoyé le code d'état"

#: searchDialog.js:164
msgid "Search input was empty!"
msgstr "Le texte saisi était vide !"

#~ msgid "Favourites"
#~ msgstr "Favoris"

#~ msgid "Channels"
#~ msgstr "Canaux"

#~ msgid "Add Channel"
#~ msgstr "Ajouter Canal"
